package ictgradschool.industry.designpatterns.ex01;

import java.util.ArrayList;

public class NestingShape extends Shape {

    ArrayList<Shape> shape = new ArrayList<Shape>();


    public NestingShape() {
        super();
    }

    public NestingShape(int x, int y) {
        super(x, y);
    }


    public NestingShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height) {
        super.move(width, height);
        for (Shape childshape : shape) {
            childshape.move(this.fWidth, this.fHeight);
        }
    }

    public void paint(Painter painter) {
        painter.drawRect(this.fX, this.fY, this.fWidth, this.fHeight);
        painter.translate(this.fX, this.fY);
        for (Shape childshape : shape) {
            childshape.paint(painter);
        }
        painter.translate(-this.fX, -this.fY);

    }

    public void add(Shape child) throws IllegalArgumentException {
        if (shape.contains(child)) {
            throw new IllegalArgumentException();
        } else if (child.fHeight + child.fY >= this.fHeight || child.fWidth + child.fX >= this.fWidth) {
            throw new IllegalArgumentException();
        } else if (child.parent != null) {
            throw new IllegalArgumentException();
        }
        shape.add(child);
        child.setParent(this);
    }

    public void remove(Shape child) {
        if (shape.contains(child)) {
            shape.remove(child);
            child.setParent(null);
        }
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException {
        Shape position = null;
        position = shape.get(index);
        return position;
    }

    public int shapeCount() {
        return shape.size();
    }

    public int indexOf(Shape child) {
        return shape.indexOf(child);
    }

    public boolean contains(Shape child) {
        return shape.contains(child);
    }
}


//
//    shapes . add ( new RectangleShape ( 0 , 0 , 2 , 3 ));
//shapes . add ( new RectangleShape ( 10 , 10 , 5 , 7 ));
//shapes . add ( new OvalShape ( 15 , 50 , 3 , 2 , 40 , 40 ));
//shapes . add ( new GemShape ( 100 , 200 , 3 , 2 , 100 , 40 ));
//shapes . add ( new GemShape ( 200 , 100 , 4 , 5 , 40 , 40 ));
//shapes . add ( new DynamicRectangleShape ( 300 , 200 , 5 , 7 , 30 , 50 , Color . red ));
//    NestingShape topLevelNest = new NestingShape ( 0 , 0 , 2 , 2 , 200 , 200 );
//    NestingShape midLevelNest = new NestingShape ( 0 , 0 , 2 , 2 , 100 , 100 );
//    NestingShape bottomLevelNest = new NestingShape ( 5 , 5 , 2 , 2 , 75 , 75 );
//    ImageShape tRex = new ImageShape ( 20 , 20 , 3 , 4 , "TRex.png" , 0.2 );
//    RectangleShape simpleShape = new RectangleShape ( 1 , 1 , 1 , 1 , 5 , 5 );
//midLevelNest . add ( bottomLevelNest );
//midLevelNest . add ( tRex );
//topLevelNest . add ( midLevelNest );
//bottomLevelNest . add ( simpleShape );
//shapes . add ( topLevelNest );