package ictgradschool.industry.designpatterns.ex02;

import ictgradschool.industry.designpatterns.ex01.NestingShape;
import ictgradschool.industry.designpatterns.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

public class ShapeAdapter implements TreeModel {

    private Shape adaptee;


    public ShapeAdapter(Shape root) {
        adaptee = (NestingShape) root;
    }


    @Override
    public Object getRoot() {
        return adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object result = null;

        if (parent instanceof NestingShape) {
            NestingShape shape = (NestingShape) parent;
            result = shape.shapeAt(index);

//            NestingShape directory = (NestingShape) shape;
//            result = directory.getNumberOfChildren();
        }
        return result;
    }

    @Override
    public int getChildCount(Object parent) {
        int childcount = 0;

        if (parent instanceof NestingShape) {
            NestingShape shape = (NestingShape) parent;
            childcount = shape.shapeCount();
        }
        return childcount;
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }


//    don't change the one below
    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {


    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {

        if (parent instanceof NestingShape && child instanceof Shape) {
            NestingShape shape = (NestingShape) parent;
            Shape littleone = (Shape) child;
           return shape.indexOf(littleone);
        }
            return -1;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }
}
